import datetime
import torch

from aeinet.aei import FaceShift


def main():
    load_start = datetime.datetime.utcnow()
    shifter = FaceShift()
    load_end = datetime.datetime.utcnow()
    input_tensor_1 = torch.load('aei_input_tensor.pt', map_location=torch.device('cpu'))
    input_tensor_2 = torch.load('aei_input_2.pt', map_location=torch.device('cpu'))
    run_start = datetime.datetime.utcnow()
    result = shifter.run(input_tensor_1, input_tensor_2)
    run_end = datetime.datetime.utcnow()
    print("Model load time: ", (load_end - load_start).total_seconds())
    print("Model inference time: ", (run_end - run_start).total_seconds())
    return result


if __name__ == "__main__":
    main()
