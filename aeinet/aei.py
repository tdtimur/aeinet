import torch
import numpy as np

from .net import AEI_Net


class FaceShift:
    def __init__(self):
        self.model = AEI_Net("unet", num_blocks=2, c_id=512)
        self.model.eval()
        self.model.load_state_dict(
            torch.load("weights/G_unet_2blocks.pth", map_location=torch.device("cpu"))
        )
        # self.model = self.model.half()

    def run(self, source_emb: torch.tensor, target: torch.tensor) -> np.ndarray:
        """
        Apply faceshifter model for batch of target images
        """
        if source_emb.dtype == torch.float16:
            source_emb = source_emb.to(torch.float32)
        if target.dtype == torch.float16:
            target = target.to(torch.float32)

        bs = target.shape[0]
        assert target.ndim == 4, "target should have 4 dimentions -- B x C x H x W"

        if bs > 1:
            source_emb = torch.cat([source_emb] * bs)

        with torch.no_grad():
            Y_st, _ = self.model(target, source_emb)
            Y_st = (Y_st.permute(0, 2, 3, 1) * 0.5 + 0.5) * 255
            Y_st = Y_st[:, :, :, [2, 1, 0]].type(torch.uint8)
            Y_st = Y_st.cpu().detach().numpy()
        return Y_st
